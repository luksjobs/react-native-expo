### Aplicativo Uni APP

Aplicativo com o objetivo de gerenciar a busca de consultas, e procedimentos médicos de forma rápida e eficiente;

# Instalação 

```
npm install
```

Logo após de instalar as depedências do projeto, será necessário instalar o expo:

```
npx expo install
``` 

Com o expo instalado, será necessário agora buildar o projeto para que assim possa integrar ao Firebase:

```
npx expo prebuild
```
Agora que o projeto foi buildado e possivelmente conectado a nossa api que está no Firwebase, é só rodar o projeto:

```
expo start
```

# Telas do Projeto

![Tela de Login](https://i.ibb.co/vkYBNKX/teladelogin.png)

![Tela de Cadastro](https://i.ibb.co/0tL3SBg/telacadastro.png)

![Tela de Geo Localização](https://i.ibb.co/hsZQdxL/telageolocation.png)